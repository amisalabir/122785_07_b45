<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/2/2017
 * Time: 4:09 PM
 */

//while loop
$i=0;
while ($i<10){
    echo "Hello <br>";
    $i=$i+1;
}//end of while loop
   // echo "Hello <br>";


//do while
$i=35;

do{
        echo "$i"."<br>";

} while($i<10);


$x=1;
for( ; $x<=10 ; ){

    echo $x++. " ";
}

$start=1;
for($start; $start<=1000; $start++){

    if(($start%3==0) && ($start)%5==0){

        echo $start."<br>";
    }
}
?>